
(function capsule(window) {
  function defineLibrary() {
    const FrameBufferLib = {};
    FrameBufferLib.texturecounter = 0;

    FrameBufferLib.TextureUnits = [];

    FrameBufferLib.init = function init(gl) {
      FrameBufferLib.TextureUnits = [
        gl.TEXTURE0,
        gl.TEXTURE1,
        gl.TEXTURE2,
        gl.TEXTURE3,
        gl.TEXTURE4,
        gl.TEXTURE5,
        gl.TEXTURE6,
        gl.TEXTURE7,
        gl.TEXTURE8,
        gl.TEXTURE9,
        gl.TEXTURE10,
        gl.TEXTURE11,
        gl.TEXTURE12,
        gl.TEXTURE13,
        gl.TEXTURE14,
        gl.TEXTURE15,
        gl.TEXTURE16,
        gl.TEXTURE17,
        gl.TEXTURE18,
        gl.TEXTURE19,
        gl.TEXTURE20,
        gl.TEXTURE21,
        gl.TEXTURE22,
        gl.TEXTURE23,
        gl.TEXTURE24,
        gl.TEXTURE25,
        gl.TEXTURE26,
        gl.TEXTURE27,
        gl.TEXTURE28,
        gl.TEXTURE29,
        gl.TEXTURE30,
        gl.TEXTURE31

      ];
    };

    FrameBufferLib.createFramebuffer = function createFramebuffer(gl, texture, canvas) {
      const framebuffer = gl.createFramebuffer();
      gl.bindFramebuffer(gl.FRAMEBUFFER, framebuffer);
      framebuffer.width = canvas.width;
      framebuffer.height = canvas.height;
      gl.bindTexture(gl.TEXTURE_2D, texture);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, framebuffer.width,
        framebuffer.height, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
      const renderbuffer = gl.createRenderbuffer();
      gl.bindRenderbuffer(gl.RENDERBUFFER, renderbuffer);
      gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16,
          framebuffer.width, framebuffer.height);
      gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0,
        gl.TEXTURE_2D, texture, 0);
      gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT,
        gl.RENDERBUFFER, renderbuffer);
      gl.bindTexture(gl.TEXTURE_2D, null);
      gl.bindRenderbuffer(gl.RENDERBUFFER, null);
      gl.bindFramebuffer(gl.FRAMEBUFFER, null);
      return framebuffer;
    };


    FrameBufferLib.createDataFramebufferRG = function createDataFramebufferRG(gl, texture, w, h) {
      const fb = gl.createFramebuffer();
      gl.bindFramebuffer(gl.FRAMEBUFFER, fb);
      fb.width = w;
      fb.height = h;
      gl.bindTexture(gl.TEXTURE_2D, texture);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RG32F, fb.width, fb.height, 0, gl.RG, gl.FLOAT, null);
      const renderbuffer = gl.createRenderbuffer();

      gl.bindRenderbuffer(gl.RENDERBUFFER, renderbuffer);
      gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texture, 0);
      gl.bindTexture(gl.TEXTURE_2D, null);
      gl.bindRenderbuffer(gl.RENDERBUFFER, null);
      gl.bindFramebuffer(gl.FRAMEBUFFER, null);
      return fb;
    };

    FrameBufferLib.createDataFramebufferRGBA = function createDataFramebufferRGBA(gl, texture, w, h) {
      const fb = gl.createFramebuffer();
      gl.bindFramebuffer(gl.FRAMEBUFFER, fb);
      fb.width = w;
      fb.height = h;
      gl.bindTexture(gl.TEXTURE_2D, texture);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA32F, fb.width, fb.height, 0, gl.RGBA, gl.FLOAT, null);
      const renderbuffer = gl.createRenderbuffer();

      gl.bindRenderbuffer(gl.RENDERBUFFER, renderbuffer);
      gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texture, 0);
      gl.bindTexture(gl.TEXTURE_2D, null);
      gl.bindRenderbuffer(gl.RENDERBUFFER, null);
      gl.bindFramebuffer(gl.FRAMEBUFFER, null);
      return fb;
    };

    FrameBufferLib.getNextTextureUnit = function getNextTextureUnit() {
      const nu = FrameBufferLib.texturecounter;
      const tu = FrameBufferLib.TextureUnits[FrameBufferLib.texturecounter];
      FrameBufferLib.texturecounter += 1;
      return ({ unit: tu, number: nu });
    };
    return FrameBufferLib;
  }
// define globally if it doesn't already exist
  if (typeof (FrameBufferLib) === 'undefined') {
    const mwindow = window;
    mwindow.FrameBufferLib = defineLibrary();
  }
}(window)); // eslint-disable-line no-undef
