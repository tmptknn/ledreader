/* eslint no-loop-func: "error"*/
/* eslint-env es6*/
/* eslint-disable require-jsdoc*/
(function libraryWrapper(window) {
  function defineLibrary() {
    let canvas;
    let ctx;
    let sync = 0;
    const frequency = SyncValuesLib.frequency;
    let bitti = 0;

    const data = '{m:"Red Leader Standing By!"}';
    let currentBit = 0;
    let currentChar = 0;

    const FlasherLib = {};
    function drawRed() {
      ctx.fillStyle = '#8f0000';
      ctx.fillRect(0, 0, canvas.width, canvas.height);
    }

    function drawBlack() {
      ctx.fillStyle = '#000000';
      ctx.fillRect(0, 0, canvas.width, canvas.height);
    }

    function sendData() {
      // if (currentChar < data.length - 1) {
      const chacha = data.charCodeAt(currentChar);
      bitti = ((chacha >> currentBit) ^ sync) & 1;
      sync ^= 1;
      if (sync == 0) {
        currentBit++;
      }
      // console.log(`${chacha} ${bitti}`);
      if (bitti === 1) {
        drawRed();
      } else {
        drawBlack();
      }

      if (currentBit > 7) {
          // console.log('|');
        currentBit = 0;
        currentChar++;
        if (currentChar >= data.length) {
          currentChar = 0;
        }
      }
    }


    FlasherLib.start = function start() {
      canvas = document.getElementById('canvas'); // eslint-disable-line no-undef
      console.log(`Canvas width ${canvas.width} height ${canvas.height}`);
      ctx = canvas.getContext('2d');
      drawRed();
      setInterval(sendData, frequency);
    };

    return FlasherLib;
  }
  if (typeof (FlasherLib) === 'undefined') window.FlasherLib = defineLibrary(); // eslint-disable-line no-param-reassign, no-undef
  else console.log('Library already defined.'); // eslint-disable-line no-console
}(window)); // eslint-disable-line no-undef
