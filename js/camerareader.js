(function libraryWrapper(window) {
  function defineLibrary() {
    const CameraReaderLib = {};

    class Filter {

      makebuffers(gl) {
        this.vbuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vbuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([-1, -1, 1, -1, -1, 1, 1, 1
        ]), gl.STATIC_DRAW);
        this.tbuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.tbuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([
          0, 0,
          1, 0,
          0, 1,
          1, 1
        ]), gl.STATIC_DRAW);
        this.ibuffer = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.ibuffer);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array([0, 1, 2, 1, 2, 3,
        ]), gl.STATIC_DRAW);
      }

      getVertexShader() {
        this.vertexShader = `#version 300 es
precision highp float;
in vec2 vPosition;
in vec2 texCoordinate;
out vec2 v_texCoordinate;
void main() {
v_texCoordinate = texCoordinate;
gl_Position = vec4(vPosition,0.0,0.0);
}`;
      }

      getFragmentShader() {
        this.fragmentShader = `#version 300 es
precision highp float;
uniform sampler2D uTexture0;
//uniform vec2 uDataSize;
in vec2 v_texCoordinate;
out vec4 fragColor;

void main() {
fragColor = texture(uTexture0, v_texCoordinate);
}`;
      }

      getExtraParams(gl, c) {
        this.params.texCoordinate = gl.getAttribLocation(this.program, 'texCoordinate');
      }

      setExtraParams(gl, c) {
        gl.bindBuffer(gl.ARRAY_BUFFER, this.tbuffer);
        gl.enableVertexAttribArray(this.params.texCoordinate);
        gl.vertexAttribPointer(this.params.texCoordinate, 2, gl.FLOAT, false, 0, 0);
      }

      constructor(gl, c) {
        this.params = {};
        this.resize(gl, c);
        this.getVertexShader();
        this.getFragmentShader();
        this.program = ShaderLib  // eslint-disable-line no-undef
.makeProgram(gl, this.vertexShader, this.fragmentShader);
        gl.useProgram(this.program);
        this.params.aPosition = gl.getAttribLocation(this.program, 'vPosition');

        this.params.uTexture0 = gl.getUniformLocation(this.program, 'uTexture0');
        this.getExtraParams(gl, c);
        this.makebuffers(gl);
      }

      resize(gl, c) {
        this.fb = {};
        this.fb.texture = gl.createTexture();
        this.fb.textureUnit = FrameBufferLib.getNextTextureUnit();
        this.fb.frameBuffer = FrameBufferLib.createFramebuffer(gl, this.fb.texture, c);
      }

      bindFrameBuffer(gl) {
        gl.bindFramebuffer(gl.FRAMEBUFFER, this.fb.frameBuffer);
      }

      setViewPort(gl, canvas) {
        gl.viewport(0, 0, canvas.width, canvas.height);
      }

      draw(gl, canvas, fb) {
        this.bindFrameBuffer(gl);
        gl.useProgram(this.program);
        this.setViewPort(gl, canvas);
        gl.clearColor(0.3, 0.6, 0.95, 1.0);
    // Enable depth testing
        gl.enable(gl.DEPTH_TEST);
    // Near things obscure far things
        gl.depthFunc(gl.LEQUAL);
    // Clear the color as well as the depth buffer.
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT); // eslint-disable-line no-bitwise

        gl.activeTexture(fb.textureUnit.unit);
        gl.bindTexture(gl.TEXTURE_2D, fb.texture);
        gl.uniform1i(this.params.uTexture0, fb.textureUnit.number);

        gl.bindBuffer(gl.ARRAY_BUFFER, this.vbuffer);
        gl.enableVertexAttribArray(this.params.aPosition);
        gl.vertexAttribPointer(this.params.aPosition, 2, gl.FLOAT, false, 0, 0);


        this.setExtraParams(gl, canvas);

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.ibuffer);
        gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_SHORT, 0);
      }
}

    class VerticalFilter extends Filter {

      getVertexShader() {
        this.vertexShader = `#version 300 es
precision highp float;
in vec2 vPosition;
in vec2 texCoordinate;
out vec2 v_texCoordinate;
void main() {
v_texCoordinate = texCoordinate;
gl_Position = vec4(vPosition,0.0,0.0);
}`;
      }

      getFragmentShader() {
        this.fragmentShader = `#version 300 es
precision highp float;
uniform sampler2D uTexture0;
uniform vec2 uDataSize;
in vec2 v_texCoordinate;
out vec4 fragColor;

void main() {
float redsum = 0.0;
for(float i=0.0; i<uDataSize.y; i+=1.0){
  vec4 c =texture(uTexture0, vec2(v_texCoordinate.x, i/(uDataSize.y-1.0)));
  redsum += c.r-c.r*0.2126+c.g*0.7152+c.b*0.0722;
}
redsum =redsum/uDataSize.y;
fragColor = vec4(redsum, 0.0,0.0,1.0);
}`;
      }


      setViewPort(gl, canvas) {
        gl.viewport(0, 0, canvas.width, 1);
      }

      resize(gl, c) {
        this.fb = {};
        this.fb.texture = gl.createTexture();
        this.fb.textureUnit = FrameBufferLib.getNextTextureUnit();
        this.fb.frameBuffer = FrameBufferLib.createDataFramebufferRGBA(gl, this.fb.texture, c.width, 1);
      }

      getExtraParams(gl, c) {
        this.params.uDataSize = gl.getUniformLocation(this.program, 'uDataSize');
        this.params.texCoordinate = gl.getAttribLocation(this.program, 'texCoordinate');
      }

      setExtraParams(gl, c) {
        gl.uniform2f(this.params.uDataSize, c.width, c.height);
        gl.bindBuffer(gl.ARRAY_BUFFER, this.tbuffer);
        gl.enableVertexAttribArray(this.params.texCoordinate);
        gl.vertexAttribPointer(this.params.texCoordinate, 2, gl.FLOAT, false, 0, 0);
      }
}

    class HorizontalFilter extends Filter {

      getVertexShader() {
        this.vertexShader = `#version 300 es
precision highp float;
in vec2 vPosition;
void main() {
gl_Position = vec4(vPosition,0.0,0.0);
}`;
      }

      getFragmentShader() {
        this.fragmentShader = `#version 300 es
precision highp float;
uniform sampler2D uTexture0;
uniform vec2 uDataSize;
out vec4 fragColor;

void main() {
float redsum = 0.0;
for(float i=0.0; i<uDataSize.x; i+=1.0){
  vec4 c =texture(uTexture0, vec2(i/(uDataSize.x-1.0), 0));
  redsum += c.r;
}
redsum = redsum/uDataSize.x;
fragColor = vec4(redsum, 0.0,0.0,1.0);
}`;
      }


      setViewPort(gl) {
        gl.viewport(0, 0, 1, 1);
      }

      resize(gl, c) {
        this.fb = {};
        this.fb.texture = gl.createTexture();
        this.fb.textureUnit = FrameBufferLib.getNextTextureUnit();
        this.fb.frameBuffer = FrameBufferLib.createDataFramebufferRGBA(gl, this.fb.texture, 1, 1);
      }

      getExtraParams(gl, c) {
        this.params.uDataSize = gl.getUniformLocation(this.program, 'uDataSize');
      }

      setExtraParams(gl, c) {
        gl.uniform2f(this.params.uDataSize, c.width, 1.0);
      }
}

    class OutputFilter extends Filter {
      makebuffers(gl) {
        this.vbuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vbuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([-1, -1, 1, -1, -1, 1, 1, 1
        ]), gl.STATIC_DRAW);
        this.tbuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.tbuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([
          0, 1,
          1, 1,
          0, 0,
          1, 0
        ]), gl.STATIC_DRAW);
        this.ibuffer = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.ibuffer);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array([0, 1, 2, 1, 2, 3,
        ]), gl.STATIC_DRAW);
      }


      bindFrameBuffer(gl) {
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
      }
}

    class CameraReader {
      constructor(canvaslayer) {
        const that = this;
        this.deviceNumber = SyncValuesLib.device;
        this.cameraDevices = {};
        this.deviceIds = [];
        this.videofb = {};
        this.currentBit = 0;
        this.rawValue = 0;
        this.data = new Float32Array(4);
        this.avgm = SyncValuesLib.averageBufferSize;
        this.avgc = 0;
        this.avg = 0;
        this.avgbuffer = [];
        this.samples = SyncValuesLib.samples;
        this.buffering = true;

        this.fc = {
          framecount: 0,
          fps: 0.0,
          lastTime: performance.now(), // eslint-disable-line no-undef
          fpsString: '',  // eslint-disable-line no-undef
          fpsUpdate(fc) {
            fc.framecount++;
            if (fc.framecount > 100) {
              fc.framecount = 0;
              const now = performance.now(); // eslint-disable-line no-undef
              fc.fps = 1000.0 * (100.0 / (now - fc.lastTime));
              fc.lastTime = now;
              fc.fpsString = `fps ${fc.fps.toFixed(2)}`;
              // console.log(`fps ${fc.fps.toFixed(2)}`);
            }
          }
        };

        this.canvaslayer = canvaslayer;
        this.width = 160;
        this.height = 120;
        this.canvas = document.createElement('canvas');
        this.canvas.width = this.width;
        this.canvas.height = this.height;
        // this.canvaslayer.appendChild(this.canvas);
        this.initGL(this.canvas);
        navigator.mediaDevices // eslint-disable-line no-undef
      .enumerateDevices().then(devices => this.gotDevices(devices, that)).then(() => {
        // this.initGL(this.canvas);
      });
      }

      render() {
        const gl = this.gl;

        gl.bindTexture(gl.TEXTURE_2D, this.videofb.texture);
        if (this.video.loaded) {
          gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, this.video);
        }

        this.verticalFilter.draw(gl, this.canvas, this.videofb);
        this.horizontalFilter.draw(gl, this.canvas, this.verticalFilter.fb);
        gl.readBuffer(gl.COLOR_ATTACHMENT0);
        gl.readPixels(0, 0, 1, 1, gl.RGBA, gl.FLOAT, this.data);

        // this.outputFilter.draw(gl, this.canvas, this.horizontalFilter.fb);

        this.avgbuffer[this.avgc] = this.data[0];
        this.avgc++;
        if (this.avgc >= this.avgm) {
          this.avgc = 0;
          this.buffering = false;
        }

        this.avg = 0.0;
        for (let i = 0; i < this.avgm; i++) {
          this.avg += this.avgbuffer[i];
        }
        this.avg /= this.avgm;
        let value = 0;
        for (let i = 0; i < this.samples; i++) {
          value += (this.avgbuffer[(this.avgc + this.avgm - i) % this.avgm] > this.avg) ? 1 : 0;
        }
        value /= this.samples;
        if (value > 0.5) {
          this.rawValue = 1;
        } else {
          this.rawValue = 0;
        }
        this.fc.fpsUpdate(this.fc);

        // requestAnimationFrame(() => { this.render(); });
        this.timer = setTimeout(() => { this.render(); }, 1);
      }

      startReader() {
        const gl = this.gl;
        this.videofb.texture = gl.createTexture();
        this.videofb.textureUnit = FrameBufferLib.getNextTextureUnit();
        gl.bindTexture(gl.TEXTURE_2D, this.videofb.texture);
        if (this.video.loaded) {
          console.log('videoloaded');
          gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, this.video);
        }
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        gl.bindTexture(gl.TEXTURE_2D, null);

        this.verticalFilter = new VerticalFilter(this.gl, this.canvas);
        this.horizontalFilter = new HorizontalFilter(this.gl, this.canvas);
        this.outputFilter = new OutputFilter(this.gl, this.canvas);
        // requestAnimationFrame(() => { this.render(); });
        setTimeout(() => { this.render(); }, 1);
      }

      handleError(error) {
        if (error.name === 'ConstraintNotSatisfiedError') {
          console.log('The resolution is not suppported');
        } else if (error.name === 'PermissionDeniedError') {
          console.log('Permissions have not been granted to use your camera and ' +
    'microphone, you need to allow the page access to your devices in ' +
    'order for the demo to work.');
        }
        console.log(`getUserMedia error: ${error.name}`, error);
      }

      openDevice(stream, that) {
        const s = stream;
    // mute audio
        const audioTracks = stream.getAudioTracks();
        for (let i = 0, l = audioTracks.length; i < l; i++) {
          audioTracks[i].enabled = false;
        }

        const videoTracks = stream.getVideoTracks();
        const tracksettings = videoTracks[0].getSettings();
        that.width = (tracksettings.width) ? tracksettings.width : that.width;
        that.height = (tracksettings.height) ? tracksettings.height : that.height;
        console.log(`Using video device: ${videoTracks[0].label} width ${that.width} height ${that.height}`); // eslint-disable-line no-console
        s.oninactive = function streamactivate() {
          console.log('Stream inactive'); // eslint-disable-line no-console
        };

        const capabilities = videoTracks[0].getCapabilities();
        const settings = videoTracks[0].getSettings();
        const constraints = videoTracks[0].getConstraints();
        const supports = navigator.mediaDevices.getSupportedConstraints();

        console.log(JSON.stringify(capabilities));
        console.log(JSON.stringify(settings));
        console.log(JSON.stringify(constraints));
        console.log(JSON.stringify(supports));

        that.video.srcObject = stream;
        const vidia = that.video;
        vidia.onplaying = function () {
          console.log('On play filter');
          vidia.loaded = true;
          that.startReader();
        };
        that.canvaslayer.appendChild(vidia); // add video preview
        that.video.play();
      }


      gotDevices(deviceInfos, that) {
        that.cameraDevices = {};
        for (let i = 0; i !== deviceInfos.length; ++i) {
          const deviceInfo = deviceInfos[i];
          const id = deviceInfo.deviceId;
          if (deviceInfo.kind === 'audioinput') {
          } else if (deviceInfo.kind === 'audiooutput') {
          } else if (deviceInfo.kind === 'videoinput') {
            const text = deviceInfo.label;
            that.deviceIds.push(deviceInfo.deviceId);
            that.cameraDevices[deviceInfo.deviceId] = deviceInfo;
            console.log(`Camera ${text} id ${id}`); // eslint-disable-line no-console
          } else {
            console.log('Some other kind of source/device: ', deviceInfo); // eslint-disable-line no-console
          }
        }

        that.video = document.createElement('video'); // eslint-disable-line no-undef
        that.video.setAttribute('width', that.width);
        that.video.setAttribute('height', that.height);

        // }
        const con = { video: { deviceId: that.deviceIds[that.deviceNumber], width: that.width, height: that.height, frameRate: 120, advanced: [{ exposureMode: 'manual', exposureTime: 0.005 }] } };

        navigator.mediaDevices.getUserMedia(con) // eslint-disable-line no-undef
    .then(stream => that.openDevice(stream, that)).catch(that.handleError);
      }


      initGL() {
        this.gl = this.canvas.getContext('webgl2', {
          premultipliedAlpha: false  // Ask for non-premultiplied alpha
        });
        const ext = this.gl.getExtension('EXT_color_buffer_float');
        if (!ext) {
          alert('need EXT_color_buffer_float');
          return;
        }
        const alignment = 1;
        this.gl.pixelStorei(this.gl.UNPACK_ALIGNMENT, alignment);
        this.gl.pixelStorei(this.gl.PACK_ALIGNMENT, alignment);
        FrameBufferLib.init(this.gl);
        this.gl.viewport(0, 0, this.width, this.height);
      }

      close() {
        this.canvaslayer.removeChild(this.video);
        if (this.timer) {
          clearTimeout(this.timer);
        }
        // this.canvaslayer.removeChild(this.canvas);
      }
}

    CameraReaderLib.CameraReader = CameraReader;

    return CameraReaderLib;
  }
  if (typeof (CameraReaderLib) === 'undefined') window.CameraReaderLib = defineLibrary(); // eslint-disable-line no-param-reassign, no-undef
  else console.log('Library already defined.'); // eslint-disable-line no-console
}(window)); // eslint-disable-line no-undef
