(function libraryWrapper(window) {
  function defineLibrary() {
    SyncValuesLib = {};
    SyncValuesLib.device = 0;
    SyncValuesLib.frequency = 200;
    SyncValuesLib.averageBufferSize = 400;
    SyncValuesLib.samples = 3;
    SyncValuesLib.setDevice = function SetDevice(d) {
      console.log(`device set to ${d}`);
      SyncValuesLib.device = d;
    };
    return SyncValuesLib;
  }
  if (typeof (SyncValuesLib) === 'undefined') window.SyncValuesLib = defineLibrary(); // eslint-disable-line no-param-reassign, no-undef
  else console.log('Library already defined.'); // eslint-disable-line no-console
}(window)); // eslint-disable-line no-undef
