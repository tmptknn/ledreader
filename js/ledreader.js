/* eslint no-loop-func: "error"*/
/* eslint-env es6*/
/* eslint-disable require-jsdoc*/
(function libraryWrapper(window) {
  function defineLibrary() {
    let cameraReader;
    let sync = 0;
    const LedReaderLib = {};
    let c = -1;
    const data = new Uint16Array(800);
    let textElem;
    let syncElem;
    let bitCount = 0;
    let bitOffset = 0;
    let string = '';
    let lastBit;
    let interval;
    let synced = false;
    let bitString = '';
    const frequency = SyncValuesLib.frequency;
    const waitTime = Math.floor(frequency / 2) - 1;// Math.floor(frequency / 2 - 6);

    function readData() {
      c = cameraReader.rawValue ^ sync;
      if (c === lastBit && sync === 1) {
        // console.log('out of sync');
        syncElem.style.color = 'red';
        synced = false;
        clearInterval(interval);
        lastBit = c ^ 1;
        setTimeout(() => { interval = setInterval(readData, frequency); }, waitTime);
        return;
      }
      if (!synced) {
        syncElem.style.color = 'green';
        synced = true;
      }
      lastBit = c ^ 1;
      sync ^= 1;
      if (sync === 1 && !cameraReader.buffering) {
        data[bitCount % 800] = c;


        bitCount++;
        if (bitCount % 8 == 0) makeString();
      }
    }

    function makeString() {
      string = '';
      bitString = '';
      let chacha = 0;
      for (let i = 0; i < bitCount - bitOffset; i++) {
        const bit = data[i + bitOffset];
        bitString = bitString.concat(bit);
        chacha |= bit << (i % 8);
        if (i % 8 === 7) {
          string = string.concat(String.fromCharCode(chacha));
          chacha = 0;
        }
      }
      textElem.innerHTML = string.concat('<br>').concat(bitString).concat('<br>').concat(cameraReader.fc.fpsString);
    }

    function keydown(event) {
      switch (event.key) {
        case 'ArrowLeft':
          LedReaderLib.offsetMinus();
          break;
        case 'ArrowRight':
          LedReaderLib.offsetPlus();
          break;
        default:
          console.log(event.key);
          makeString();
          break;
      }
    }
    LedReaderLib.offsetMinus = function offsetMinus() {
      if (bitOffset == 0) return;
      bitOffset -= 1;
      makeString();
    };

    LedReaderLib.offsetPlus = function offsetPlus() {
      bitOffset += 1;
      makeString();
    };

    LedReaderLib.start = function start() {
      textElem = document.getElementById('text');
      syncElem = document.getElementById('sync');
      if (cameraReader != null) {
        cameraReader.close();
      }
      cameraReader = new CameraReaderLib.CameraReader(document.getElementById('canvaslayer'));
      document.addEventListener('keydown', keydown);
      if (interval) {
        clearInterval(interval);
      }
      interval = setInterval(readData, frequency);
    };

    return LedReaderLib;
  }
  if (typeof (LedReaderLib) === 'undefined') window.LedReaderLib = defineLibrary(); // eslint-disable-line no-param-reassign, no-undef
  else console.log('Library already defined.'); // eslint-disable-line no-console
}(window)); // eslint-disable-line no-undef
